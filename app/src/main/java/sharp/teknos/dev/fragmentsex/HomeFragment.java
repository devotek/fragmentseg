package sharp.teknos.dev.fragmentsex;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {


    private ImageView imageView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_home, container, false);

        View view = (View) inflater.inflate(R.layout.fragment_home,container,false);
        imageView = (ImageView)view.findViewById(R.id.imageView);
        Button loadBtn = (Button)view.findViewById(R.id.loadImgBtn);
        loadBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        //
        Picasso.with(getContext()).
                load("https://melbournechapter.net/images/elephant-clipart-10.png").placeholder(R.drawable.loading).
                into(imageView);

    }
}
